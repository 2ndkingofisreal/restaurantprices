# Description

The present module is a implementation of a simple Perceptron algorithm for classifying restaurant data.

# Requirements

* Python >= 3.6
* NLTK >= 3.3 (download of additional ressources within NLTK might be necessary to perform POS-tagging)
* scikit-learn >= 0.19.2 (only for experiments)
* statsmodels >= 0.9.0 (only for experiments)

# Usage

The funcionality of the perceptron classifier is interface via the _model.py_ script.
It provides two main functionalities: training and saving a model (_model_to_) and loading a previously trained model and perform classificitaion (_model_from_).
(Help information can be accessed by calling the script with the `-h` flag)

## Training a model

The following positional arguments are required:

* model_output: The file to which the model will be saved as a pickled file
* training_file: The path to the training data
* dev_file: Path to the development data
* iterations: Number of iterations the model will be trained with

Example run:

```
> model.py model_to model.pickle menu_train.txt menu_dev.txt 60
```
## Testing a model

The following positional arguments are required:

* model_input: The file containing the previously trained model in pickle format
* teset_file: Path to the test data
* prediction_output: The filename the prediction will be saved under.

Example run:

```
> model.py model_from model.pickle menu_test.txt prediction.txt
```

# Experiments

Using, the _experiments.py_ script, a feature addition and subtraction experiment can be run. 

## Feature extraction

Before running the experiments, it is recommended to use the _extract_features.py_ script to perform feature extraction on the training and test data **once** and output the resulting data as pickled file. Thus, time-consuming feature extraction has to performed only one. The data can be passed in pickled (binary) form to the _experiments.py_ script using the flag `-p`.
(Help information available)

```
> extract_features.py menu_train.txt menu_train_xtracted.pickle
```

## Running the experiments

Experiments can be run using the _experiments.py_ script.

Required arguments are:
* the experiment type ("addition" or "subtraction")
* Training data (raw or with features already extracted using the script above)
* Test data (raw or with features already extracted using the script above)

Option arguments
* `-p` or `--pickled` if data is passed in binary format with features already extracted

**Note:** Feature extraction takes some time! It is recommended to extract features beforehand.

With extracted data:
```
> experiments.py addition -p menu_train_xtracted.pickle menu_test_xtracted.pickle
```

without pre-extracted data:

```
> experiments.py addition menu_train.txt menu_test.txt
```

Results are written to a .csv-file.
