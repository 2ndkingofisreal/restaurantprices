#! /usr/bin/python3

import data
import pprint
import pickle
from restaurant import Restaurant

def main(corpus_path, output):
    corpus = data.Corpus(corpus_path)

    length = len(corpus.restaurants)
    counter = 0

    for restaurant in corpus.restaurants:
        counter += 1
        restaurant.getFeatures()
        print("{} / {}".format(
            counter,
            length))

    with open(output, "wb") as pickle_obj:
        pickle.dump(corpus, pickle_obj)

    print("Pickled corpus with extracted features to " + output)


if __name__ == "__main__":
    import argparse

    argparser = argparse.ArgumentParser()
    argparser.add_argument("corpus")
    argparser.add_argument("out")

    args = argparser.parse_args()

    main(args.corpus, args.out)
