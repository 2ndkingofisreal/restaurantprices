#! /usr/bin/python3

import data, evaluate, perceptron
import pickle

def model_to(args):
    train_corpus = data.Corpus(args.training_file)
    dev_corpus = data.Corpus(args.dev_file)
    p = perceptron.Perceptron(train_corpus)
    print("Initialize features and weights ...")
    print("(grab some coffee, this might take a while ...)")
    p.initialize()
    print("Done.")
    print("Train perceptron on {} with {} iterations".format(args.training_file, str(args.iterations)))
    p.train(int(args.iterations), dev_corpus, verbose=args.verbose)
    print("Finished training")
    p.save_model(args.model_output)
    print("Model saved to " + args.model_output)


def model_from(args):

    with open(args.model_input, "rb") as pickle_obj:
        weights = pickle.load(pickle_obj)

    test_corpus = data.Corpus(args.test_file)

    clf = perceptron.Classifier(test_corpus, weights)
    clf.classify()

    evaluator = evaluate.Evaluator(test_corpus)
    evaluator.print_evaluation_verbose()

    test_corpus.save_predictions(args.prediction_output)
    

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Train the perceptron classifier')
    subparsers = parser.add_subparsers()
    parser_model_to = subparsers.add_parser("model_to", help="Train a model and save it to a pickled file.")
    parser_model_to.add_argument('model_output', help="Filename for the pickled model file.")
    parser_model_to.add_argument('training_file', help='Path to file containing training data')
    parser_model_to.add_argument('dev_file',  help='Path to the development dataset')
    parser_model_to.add_argument('iterations', help='Number of iterations')
    parser_model_to.add_argument('-v', '--verbose', help='Print detailed evaluation after each iteration', action='store_true')
    parser_model_to.set_defaults(func=model_to)

    parser_model_from = subparsers.add_parser("model_from", help="Load a previously trained model and perfrom prediction.")
    parser_model_from.add_argument("model_input", help="Filename of the previously trained and pickled model.")
    parser_model_from.add_argument("test_file", help="Path to the file containing data to be predicted")
    parser_model_from.add_argument("prediction_output", help="Filename of the output file.")
    parser_model_from.set_defaults(func=model_from)

    args = parser.parse_args()

    args.func(args)
