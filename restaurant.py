import re
import wordlists
import typing
import nltk


class Restaurant:
    """
    A class that holds restaurants, i. e. one row of data. Its methods
    are used to extract features.

    """
    def __init__(self, name, pcGold, pcPred, foodType, location, menu):
        self.name: str = name
        self.pcGold: int = pcGold
        self.pcPred: int = pcPred

        self.foodType: str = foodType
        self.location: str = location
        self.menu: list = menu
        self.wordcounts: dict = {}
        self.num_dishes: int = 0
        self.authenticity: int = 0
        self.featureDict = {'BIAS': 1}
        self.avg_word_len: int = 0
        self.num_adjs: int = 0
        self.plenty: int = 0
        self.sensory: int = 0
        self.num_preps: int = 0
        self.avg_dish_len: int = 0

        self.features_extracted = False

    def countWords(self):
        """ Get word counts for bag of words """
        for menuItem in self.menu:
            for word in menuItem.words:
                self.wordcounts['word: ' + word] = self.wordcounts.get('word: ' + word, 0) + 1
        
    def get_average_word_len(self):
        """ Get the average word length of the restaurant's menu items """
        word_count = 0
        sum_word_lengths = 0
        for menuItem in self.menu:
            for word in menuItem.words:
                word_count += 1
                sum_word_lengths += len(word)

        self.avg_word_len = int(sum_word_lengths / word_count)

    def get_num_adjectives(self):
        """ Get the number of adjectives """
        num_adjectives = 0
        for menuItem in self.menu:
            words_tagged = nltk.pos_tag(menuItem.words)
            for word in words_tagged:
                if word[1] == "JJ":
                    num_adjectives += 1
        self.num_adjs = num_adjectives
        
    def countDishes(self):
        """ Count the number of dishes on the menu """
        numDishes = 0
        for menuItem in self.menu:
                numDishes += 1

        self.num_dishes = numDishes
			
    def match_from_list(self, wordlist):
        """ Helper function to count matches from a list of words """
        count = 0
        for phrase in wordlist:
            for menuItem in self.menu:
                S = " ".join(menuItem.words)
                l = re.findall(r"\b{}\b".format(phrase), S)
                count += len(l)
        return(count)

    def countAuthenticity(self):
        """ Count matches from the 'authenticity' word list """
        self.authenticity = self.match_from_list(wordlists.tradAuthenticity)
		
    def countPlenty(self):
        """ Count matches from the 'plenty' word list """
        self.plenty = self.match_from_list(wordlists.plenty)
            
    def countChoice(self):
        """ Count matches from the 'choice' word list """
        self.choice = self.match_from_list(wordlists.choice)

    def countSensory(self):
        """ Count matches from the 'sensory' word list """
        self.sensory = self.match_from_list(wordlists.sensory)

    def countPositiveSentiment(self):
        """ Count matches from the 'positive sentiment' word list """
        self.positive = self.match_from_list(wordlists.positive_sentiment)
        
    def get_average_dish_len(self):
        """ Count the average word count for one dish on the menu """
        word_count = 0
        dish_count = 0
        for menuItem in self.menu:
            dish_count += 1
            for word in menuItem.words:
                word_count += 1
        self.avg_dish_len = int(word_count / dish_count)
        
    def get_num_prepositions(self):
        """ count the number of prepositions in the menu """
        num_prepositions = 0
        for menuItem in self.menu:
            words_tagged = nltk.pos_tag(menuItem.words)
            for word in words_tagged:
                if word[1] == "IN":
                    num_prepositions += 1
        self.num_preps = num_prepositions
        
    def getFeatures(self, feats=[
        countWords,
        get_average_word_len,
        countDishes,
        countAuthenticity,
        countPlenty,
        countChoice,
        countSensory,
        countPositiveSentiment,
        get_num_adjectives,
        get_num_prepositions,
        get_average_dish_len]):
        """ Perform feature extraction """

        if self.features_extracted == False:
            for f in feats:
                f(self)

            self.features_extracted = True

    def return_features(self, feature_names):
        """ Return the features specified in a list passed as a parameter """
        if feature_names is None:
            feature_names=[
                'bow', 
                'avg_word_len', 
                'num_dishes',
                'authenticity',
                'plenty',
                'choice',
                'sensory',
                'positive',
                'num_adj',
                'num_preps',
                'avg_dish_len',
                'foodType']

        feature_dict = dict()

        for f in feature_names:
            if f == "bow":
                for w, c in self.wordcounts.items():
                    feature_dict[w] = c

            if f == "avg_word_len":
                feature_dict["avg_word_len"] = self.avg_word_len

            if f == "num_dishes":
                feature_dict["num_dishes"] = self.num_dishes

            if f == "authenticity":
                feature_dict["authenticity"] = self.authenticity

            if f == "plenty":
                feature_dict["plenty"] = self.plenty

            if f == "choice":
                feature_dict["choice"] = self.choice

            if f == "sensory":
                feature_dict["sensory"] = self.sensory

            if f == "positive":
                feature_dict["positive"] = self.positive

            if f == "num_adj":
                feature_dict["num_adj"] = self.num_adjs

            if f == "num_preps":
                feature_dict["num_preps"] = self.num_preps

            if f == "avg_dish_len":
                feature_dict["avg_dish_len"] = self.avg_dish_len

            if f == "food_type":
                feature_dict["food_type: " + self.foodType] = 1

        return feature_dict
