import data

class Evaluator:
    """ A class for creating and outputting evaluations """

    def __init__(self, corpus):
        # we want to calculate micro and macro average
        self.fscores:list = []
        self.precisions:list = []
        self.recalls:list = []

        # true positives, false positives and false negatives are stored
        # for each category (list index = price category - 1)
        self.tps:list = [0,0,0,0]
        self.fps:list = [0,0,0,0]
        self.fns:list = [0,0,0,0]

        self.isCorrect:list = []

        for restaurant in corpus.restaurants:
            if restaurant.pcGold == restaurant.pcPred:
                self.isCorrect.append(1)
            else:
                self.isCorrect.append(0)
            for pc in range(1,5):
                # Prediction and gold data match for the targeted category: true positive
                if restaurant.pcGold == pc and restaurant.pcPred == pc:
                    self.tps[pc-1] += 1

                # Gold data is targeted category, prediction is not: false negative
                elif restaurant.pcGold == pc and restaurant.pcPred != pc:
                    self.fns[pc-1] += 1

                # Prediction is targeted category, but gold is not: false postive
                elif restaurant.pcGold != pc and restaurant.pcPred == pc:
                    self.fps[pc-1] += 1

        for pc in range(0,4):
            if self.tps[pc] + self.fps[pc] == 0:
                self.precisions.append(1)
            else:
                self.precisions.append(self.tps[pc] / (self.tps[pc] + self.fps[pc]))
            if self.tps[pc] + self.fns[pc] == 0:
                self.recalls.append(0)
            else:
                self.recalls.append(self.tps[pc] / (self.tps[pc] + self.fns[pc]))

            if self.precisions[pc] != 0 and self.recalls[pc] != 0:
                self.fscores.append(2 * ( (self.precisions[pc] * self.recalls[pc]) / (self.precisions[pc] + self.recalls[pc]) ))
            else:
                self.fscores.append(0)


        self.microAverage = sum(self.fscores)/len(self.fscores)

        if sum(self.tps) + sum(self.fps) == 0:
            precisionMacro = 1
        else:
            precisionMacro = sum(self.tps) / ( sum(self.tps) + sum(self.fps) )

        if sum(self.tps) + sum(self.fns) == 0:
            recallMacro = 0
        else:
            recallMacro = sum(self.tps) / ( sum(self.tps) + sum(self.fns) )

        self.macroAverage = (2 * (precisionMacro * recallMacro) / (precisionMacro + recallMacro))

    def print_evaluation_verbose(self):
        for i in range(1,5):
            outputString = "P{category}: {precision:.2f}, R{category}: {recall:.2f}, F{category}: {fscore:.2f} | ".format(
                    category=str(i), 
                    precision=round(self.precisions[i-1] * 100,2), 
                    recall=round(self.recalls[i-1] * 100, 2), 
                    fscore=round(self.fscores[i-1] * 100, 2))
            print(outputString, end='')
        print("MicroAv: {microAverage}, MacroAv: {macroAverage}".format(
            microAverage=round(self.microAverage*100,2), 
            macroAverage=round(self.macroAverage*100,2)))

    def print_fscores(self):
        print("MicroAv: {microAverage}, MacroAv: {macroAverage}".format(
            microAverage=round(self.microAverage*100,2), 
            macroAverage=round(self.macroAverage*100,2)))

    def as_dict(self):
        results_dict = dict()
        for i in range(1,5):
            results_dict["cat" + str(i) + "-precision"] = round(self.precisions[i-1] * 100,2)
            results_dict["cat" + str(i) + "-recall"] = round(self.recalls[i-1] * 100,2)
            results_dict["cat" + str(i) + "-fscore"] = round(self.fscores[i-1] * 100,2)
        results_dict["MacroAverage"] = round(self.macroAverage * 100,2)
        results_dict["MicroAverage"] = round(self.microAverage * 100,2)

        return results_dict


def main(gold_path, model_path):
    gold = data.Corpus(gold_path)
    gold.loadPredictions(model_path)

    evaluator = Evaluator(gold)
    evaluator.printEvaluation()

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Evaluation for resturant prices-datasets")
    parser.add_argument('gold', help='Path to gold data')
    parser.add_argument('model', help='Path to model output')
    args = parser.parse_args()

    main(args.gold, args.model)
