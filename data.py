import evaluate
import restaurant

"""
This module contains the basic datatypes that 
"""

class MenuItem:
    """ A class that holds the words describing one menu item """
    def __init__(self, words):
        self.words: list = words


class Corpus:
    """ A class that holds an entire Corpus of restaurant data """
    def __init__(self, path):

        self.restaurants: list = []

        # a boolean that shows wheter predictions were loaded or not
        self.predictionsLoaded: bool = False

        with open(path, 'r') as fileObject:
            lines: list = fileObject.read().splitlines()

        for line in lines:
            menuItems: list = []
            data: list = line.split("\t")

            # menu items separated by semicolon
            # individual words separated by whitespace
            menu: list = data[4].split(";")
            for item in menu:
                if item != "":
                    menuItems.append(MenuItem(item.split(" ")))
            pc: int = len(data[0])
            assert pc > 0 and pc < 5

            # instantiate a new restaurant object and add it to the
            # restaurants attribute
            self.restaurants.append(restaurant.Restaurant(
                        data[1],
                        pc,
                        0,
                        data[2],
                        data[3],
                        menuItems))

    def load_predictions_from_file(self, predictions):
        """ load predictions from an external file """

        with open(predictions, 'r') as fileObject:
            lines: list = fileObject.read().splitlines()

        for i in range(len(lines)):
            self.restaurants[i].pcPred: int = len(lines[i])

        self.predictionsLoaded = True

    def save_predictions(self, filename):
        """ Saves predictions to a file """
        with open(filename, "w") as output_obj:
            for restaurant in self.restaurants:
                output_obj.write(restaurant.pcPred * "$" + "\n")
