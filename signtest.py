import evaluate
from statsmodels.stats.descriptivestats import sign_test

class Sign:
    def resultOld(self, evaluator):
        self.result1 = evaluator.isCorrect
    
    def resultNew(self, evaluator):
        self.result2 = evaluator.isCorrect

    def test(self):
        self.signs = []
        for x in range(len(self.result1)):
                self.signs.append(self.result2[x]-self.result1[x])
        self.M, self.p_value = sign_test(self.signs)
        return self.p_value
