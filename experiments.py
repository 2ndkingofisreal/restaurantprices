from data import Corpus
from restaurant import Restaurant
import csv
import datetime
import evaluate
import perceptron
import pickle
import scikit
import signtest

from sklearn.svm import LinearSVC

from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC

from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier

today = datetime.datetime.today()
datestring = today.strftime("%Y-%m-%d")

feats_bl = ["bow"]
feats_paper = [
        "av_word_len",
        "num_dishes",
        "authenticity",
        "plenty",
        "choice",
        "sensory",
        "positive",
        "num_adjs",
]

own_features = ['food_type', 'num_preps', 'avg_dish_len']

fieldnames = [
        'classifier', 
        'configuration',
        'cat1-precision',
        'cat1-recall',
        'cat1-fscore',
        'cat2-precision',
        'cat2-recall',
        'cat2-fscore',
        'cat3-precision',
        'cat3-recall',
        'cat3-fscore',
        'cat4-precision',
        'cat4-recall',
        'cat4-fscore',
        'MicroAverage',
        'MacroAverage',
        'p-value']

classifiers = [LinearSVC(), RandomForestClassifier(), KNeighborsClassifier(), DecisionTreeClassifier()]
names = ["LinearSVC", "RandomForest", "KNeighborsClassifier", "DecisionTree"]


def test_addition(training_data, test_data, pickled):
    csv_file = open(datestring + "_feature_addition_results.csv", "w")

    sign = signtest.Sign()

    writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter="\t")
    writer.writeheader()

    if pickled:
        with open(training_data, "rb") as train_obj:
            training_corpus = pickle.load(train_obj)

        with open(test_data, "rb") as test_obj:
            test_corpus = pickle.load(test_obj)
    else:
        training_corpus = Corpus(training_data)
        test_corpus = Corpus(test_data)

    for classifier, name in zip(classifiers, names):
        clf = scikit.Classifier(classifier, training_corpus, feats_paper)

        try:
            best = clf.get_best_features()
        except:
            pass

        print("Best paper features for {}:".format(name))
        for feature in best:
            print(feature)

        experiment_counter = 0

        # test classifiers with best features + each of own features
        for feature in own_features:
            experiment_counter += 1
            if experiment_counter == 1:
                clf = scikit.Classifier(classifier, training_corpus, ['bow'] + best)
                evaluator = clf.predict(test_corpus)
                sign.resultOld(evaluator)
                results = evaluator.as_dict()
                results['classifier'] = name
                results['configuration'] = ";".join(['bow'] + best)
                writer.writerow(results)

            clf = scikit.Classifier(classifier, training_corpus, ['bow'] + best + [feature])
            evaluator = clf.predict(test_corpus)
            sign.resultNew(evaluator)
            p = sign.test()
            results = evaluator.as_dict()
            results['classifier'] = name
            results['configuration'] = ";".join(['bow'] + best + [feature])
            results['p-value'] = p
            writer.writerow(results)

    experiment_counter = 0

    for feature in own_features:
        experiment_counter += 1
        # test perceptron with best features
        if experiment_counter == 1:
            configuration = ['bow'] + best
            perc = perceptron.Perceptron(training_corpus, configuration)
            perc.initialize()
            perc.train(20)
            
            clf = perceptron.Classifier(test_corpus, perc.weights, configuration)
            clf.classify()
            evaluator = evaluate.Evaluator(test_corpus)
            sign.resultOld(evaluator)
            results = evaluator.as_dict()
            results['classifier'] = "perceptron"
            results['configuration'] = ";".join(configuration)
            writer.writerow(results)

        configuration = ['bow'] + best + [feature]
        perc = perceptron.Perceptron(training_corpus, configuration)
        perc.initialize()
        perc.train(20)
        clf = perceptron.Classifier(test_corpus, perc.weights, configuration)
        clf.classify()
        evaluator = evaluate.Evaluator(test_corpus)
        sign.resultNew(evaluator)
        p = sign.test()
        results = evaluator.as_dict()
        results['classifier'] = "perceptron"
        results['configuration'] = ";".join(configuration)
        results['p-value'] = p
        writer.writerow(results)

    csv_file.close()

def test_subtraction(training_data, test_data, pickled):
    csv_file = open(datestring + "_feature_subtraction_results.csv", "w")

    sign = signtest.Sign()

    writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter="\t")
    writer.writeheader()

    if pickled:
        with open("data/train.pickle", "rb") as train_obj:
            training_corpus = pickle.load(train_obj)

        with open("data/test.pickle", "rb") as test_obj:
            test_corpus = pickle.load(test_obj)
    else:
        training_corpus = Corpus(training_data)
        test_corpus = Corpus(test_data)


    exp1 = feats_bl + feats_paper + ['food_type', 'num_preps', 'avg_dish_len']
    exp2 = feats_bl + feats_paper + ['food_type', 'num_preps']
    exp3 = feats_bl + feats_paper + ['food_type', 'avg_dish_len']
    exp4 = feats_bl + feats_paper + ['num_preps', 'avg_dish_len']

    configurations = [exp1, exp2, exp3, exp4]

    experiment_counter = 0

    for configuration in configurations:
        experiment_counter += 1
        print("Training perceptron with configuration " + ",".join(configuration))
        # test perceptron with best features
        if experiment_counter == 1:
            perc = perceptron.Perceptron(training_corpus, configuration)
            perc.initialize()
            perc.train(20)

            clf = perceptron.Classifier(test_corpus, perc.weights)
            clf.classify()
            evaluator = evaluate.Evaluator(test_corpus)
            sign.resultOld(evaluator)
            results = evaluator.as_dict()
            results['classifier'] = "perceptron"
            results['configuration'] = ";".join(configuration)
            writer.writerow(results)
        else:
            perc = perceptron.Perceptron(training_corpus, configuration)
            perc.initialize()
            perc.train(20)

            clf = perceptron.Classifier(test_corpus, perc.weights, configuration)
            clf.classify()
            evaluator = evaluate.Evaluator(test_corpus)
            sign.resultNew(evaluator)
            p = sign.test()
            results = evaluator.as_dict()
            results['classifier'] = "perceptron"
            results['configuration'] = ";".join(configuration)
            results['p-value'] = p
            writer.writerow(results)

    csv_file.close()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="This script performs feature addition and subtraction experiments with different classifiers")

    parser.add_argument("experiment", choices=["addition", "subtraction"], help="Select the type of experiment to be conducted.")
    parser.add_argument("training_data", help="Location of the training data")
    parser.add_argument("test_data", help="Location of the test data")
    parser.add_argument("--pickled", "-p", action='store_true', help="Flag to indicate whether input data has features pre-extracted in pickle-format")

    args = parser.parse_args()

    if args.experiment == "addition":
        test_addition(args.training_data, args.test_data, args.pickled)
    elif args.experiment == "subtraction":
        test_subtraction(args.training_data, args.test_data, args.pickled)


