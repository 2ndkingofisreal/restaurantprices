from data import Corpus

from sklearn.feature_extraction import DictVectorizer
from sklearn.svm import LinearSVC
from sklearn.feature_selection import RFECV

import evaluate

class SkExtractor:
    """ An extractor that collects features and gold labels from a restaurant corpus and 
    returns them in a representation expected by SK-learn """

    def __init__(self, corpus, feature_list):
        self.vectorizer = DictVectorizer()
        self.corpus = corpus
        self.feature_list = feature_list

        features = []
        gold_labels = []

        # extract features from the corpus and append them to the feature list
        # get gold labels and append them to the gold labels list
        for restaurant in corpus.restaurants:
            restaurant.getFeatures()
            features.append(restaurant.return_features(self.feature_list))
            gold_labels.append(restaurant.pcGold)

        X = self.vectorizer.fit_transform(features)
        self.features = X
        self.gold_labels = gold_labels
        self.vocab = self.vectorizer.vocabulary_

    def extract_features(self, restaurant):
        """ Extracts features for a restaurant and returns feature representation according
        to vectorizer fitted to self.corpus """
        restaurant.getFeatures()
        return self.vectorizer.transform(restaurant.return_features(self.feature_list))

class FeatureEliminator:
    """ This class is used to perform RFECV """
    def __init__(self, estimator, features, gold_labels, vocab):
        self.selector = RFECV(estimator)
        self.selector.fit(features,gold_labels)
        self.ranking = self.selector.ranking_
        best_features = []
        for i in range(len(self.ranking)):
            for key in vocab.keys():
                if vocab[key]==i:
                    best_features.append(key)

        for i in range(len(best_features)):
            if "food_type" in best_features[i]:
                best_features[i] = "food_type"

        self.best_features = list(set(best_features))


class Classifier:
    """ This class holds a classifier from scikit-learn. It is trained on
    the corpus it is initialized with. Its methods are used to predict categories
    and to get the best features """
    def __init__(self, classifier, training_corpus, feature_list):
        self.extractor = SkExtractor(training_corpus, feature_list)
        self.clf = classifier
        self.clf.fit(self.extractor.features, self.extractor.gold_labels)

    def predict(self, test_corpus):
        for restaurant in test_corpus.restaurants:
            class_features = self.extractor.extract_features(restaurant)
            restaurant.pcPred = self.clf.predict(class_features)[0]
        return evaluate.Evaluator(test_corpus)

    def get_best_features(self):
        eliminator = FeatureEliminator(self.clf, self.extractor.features, self.extractor.gold_labels, self.extractor.vocab)
        return eliminator.best_features