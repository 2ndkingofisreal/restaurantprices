import evaluate
import random
import pickle
from data import Corpus


class Perceptron:
    """ A class for a perceptron classifier """

    def __init__(self, training_corpus: Corpus, feature_list=None):
        """ Initialize the perceptron with baseweights """
        self.weights: list = [{}, {}, {}, {}]
        self.training_corpus = training_corpus
        self.instances = []
        self.feature_list = feature_list
        self.initialized = False

    def initialize(self):
        # get feature dict for restaurant objects if not yet calculated
        if self.initialized == False:
            weights = {}
            for restaurant in self.training_corpus.restaurants:
                if restaurant.features_extracted == False:
                    restaurant.getFeatures()

                features = restaurant.return_features(self.feature_list)
                self.instances.append( (features, restaurant.pcGold) )
                # initialize weights
                for feature in features.keys():
                    if feature not in weights.keys():
                        weights[feature] = 0

            self.weights = [weights.copy(), weights.copy(), weights.copy(), weights.copy()]
            self.initialized = True

    def train(self, iterations: int, dev_corpus=None, verbose: bool = False):
        """ Function to train the preceptron on a corpus with given number of iterations """

        for i in range(iterations):

            for features, gold_label in self.instances:
                predictions: list = [0, 0, 0, 0]
                for c in range(len(self.weights)):
                    for feature, value in features.items():
                        predictions[c] += (value * self.weights[c][feature])

                highest_prediction = predictions.index(max(predictions)) + 1

                # if prediction is not highest, adjust the weights
                if gold_label != highest_prediction:
                    for feature, value in features.items():
                        self.weights[gold_label-1][feature] += value
                        self.weights[highest_prediction-1][feature] -= value

            
            # classify dev-corpus after iteration and print evaluation, if available
            if dev_corpus is not None:
                dev_classifier = Classifier(dev_corpus, self.weights, self.feature_list)
                dev_classifier.classify()
                dev_evaluator = evaluate.Evaluator(dev_corpus)
                print("(DEV)\tI {}: |".format(str(i)), end="")
                if verbose:
                    dev_evaluator.print_evaluation_verbose()
                else:
                    dev_evaluator.print_fscores()

            # classify training_corpus and print evaluation
            train_classifier = Classifier(self.training_corpus, self.weights, self.feature_list)
            train_classifier.classify()
            train_evaluator = evaluate.Evaluator(self.training_corpus)

            print("(TRAIN)\tI {}: |".format(str(i)), end="")
            if verbose:
                train_evaluator.print_evaluation_verbose()
            else:
                train_evaluator.print_fscores()

            print("----------------------------------------------------------------------")
            # shuffle instances
            random.shuffle(self.instances)

    def save_model(self, filename):
        """ Save weights to pickled file """
        if self.initialized:
            with open(filename, "wb") as pickle_obj:
                pickle.dump(self.weights, pickle_obj)


class Classifier:

    def __init__(self, test_corpus, weights, feature_list=None):
        self.test_corpus = test_corpus
        self.weights = weights
        self.feature_list = feature_list

    def classify(self):
        """ Classification with weights on a test corpus """
        for restaurant in self.test_corpus.restaurants:
            if restaurant.features_extracted == False:
                restaurant.getFeatures()

            features = restaurant.return_features(self.feature_list)
            predictions: list = [0, 0, 0, 0]
            for c in range(len(self.weights)):
                for feature, value in features.items():
                    if feature in self.weights[c].keys():
                        predictions[c] += (value * self.weights[c][feature])

            highest_prediction = predictions.index(max(predictions)) + 1

            restaurant.pcPred = highest_prediction
